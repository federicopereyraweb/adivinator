﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Adivinator.Models;
using Adivinator004.Filters;
using Adivinator004.Models;


namespace Adivinator.Controllers
{   [FilterAdivinator]
    public class JugarController : Controller
    {
        // GET: Jugar
        public ActionResult Index()
        {
            var partida = (Partida)Session["partida"];
            HttpCookie CookieJugador = Request.Cookies[partida.jugador.Nombre];
            if(CookieJugador != null)
            {
                ViewBag.CookieNom = CookieJugador["Nombre"];
                ViewBag.CookiePuntos = CookieJugador["Puntos"];
                ViewBag.CookieGanados = CookieJugador["Ganados"];
            }     

            return View(ViewBag);
        }

        [HttpPost]
        public ActionResult Index(int num)
        {
            var partida = (Partida)Session["partida"];

            HttpCookie CookieJugador = Request.Cookies[partida.jugador.Nombre];
            if (CookieJugador != null)
            {
                ViewBag.CookieNom = CookieJugador["Nombre"];
                ViewBag.CookiePuntos = CookieJugador["Puntos"];
                ViewBag.CookieGanados = CookieJugador["Ganados"];
            }

            partida.Intentos.Add(num);
            partida.intentosPartida++;

            if (partida.NroAleatorio == num)
            {
                return RedirectToAction("Ganaste", "Jugar");
            }
          
            else if (partida.NroAleatorio > num)
            {
                ViewBag.Mensaje = "El número que ingresaste es muy bajo";
            }
            else if (partida.NroAleatorio < num)
            {
                ViewBag.Mensaje = "El número que ingresaste es muy alto";
            }
           
            

            if (partida.intentosPartida == 10)
            {
                return RedirectToAction("Perdiste", "Jugar");
            }

         

            return View(ViewBag);
        }

        public ActionResult Salir()
        {
            Session.Abandon();
            return RedirectToAction("Index", "Home");

        }

        public ActionResult NuevaPartida()
        {
            var partidaAnterior = (Partida)Session["partida"];
            Session.Remove("partida");


            Partida p = new Partida();
            p.jugador = partidaAnterior.jugador;
            p.NroAleatorio = Partida.NumeroRandom();
            p.Intentos = new List<int>();

            Session.Add("partida", p);

            return RedirectToAction("Index", "Jugar");

        }

        
        public ActionResult Ganaste()
        {
            var partida = (Partida)Session["partida"];
            partida.puntos = partida.intentosMax - partida.intentosPartida;
            partida.jugador.partidasGanadas++;
            partida.jugador.puntosJugador += partida.puntos;
            if (partida.puntos > partida.jugador.MejorMarca)
            {
                partida.jugador.MejorMarca = partida.puntos;
            }

            HttpCookie CookieJugador = Request.Cookies[partida.jugador.Nombre];
            if (CookieJugador == null)
            {
                CookieJugador = new HttpCookie(partida.jugador.Nombre);
            }



            CookieJugador["Nombre"] = partida.jugador.Nombre;
            CookieJugador["Puntos"] = (Convert.ToInt32(CookieJugador["Puntos"]) + partida.jugador.puntosJugador).ToString();
            CookieJugador["Ganados"] = (Convert.ToInt32(CookieJugador["Ganados"]) + (Convert.ToInt32("1"))).ToString();
            CookieJugador.Expires = DateTime.Now.AddDays(30);

            Response.Cookies.Add(CookieJugador);

            Juego juego = Juego.Instancia;
            juego.Ranking.Add(new KeyValuePair<string, int>(partida.jugador.Nombre, partida.jugador.puntosJugador));



            return View("Ganaste");
        }


        public ActionResult Perdiste()
        {
            return View("Perdiste");
        }
    }   
}
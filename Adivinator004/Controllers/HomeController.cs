﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Adivinator.Models;
using Adivinator004.Models;

namespace Adivinator.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home
        public ActionResult Index()
        {
            return View();
        }

        
        [HttpPost]
        public ActionResult Index(string nom)
        {
            Jugador j = new Jugador();
            j.Nombre = nom;
            Partida p = new Partida();
            p.jugador = j;
            p.NroAleatorio = Partida.NumeroRandom();
            p.Intentos = new List<int>();

 
            Juego juego = Juego.Instancia;


            Session.Add("partida", p);


            HttpCookie CookieJugador = Request.Cookies[nom];
            if(CookieJugador != null)
            {
                ViewBag.CookieNom = CookieJugador[p.jugador.Nombre];
                ViewBag.CookiePuntos = CookieJugador[p.jugador.Nombre];
                ViewBag.CookieGanados = CookieJugador[p.jugador.Nombre];
            }

            


            return RedirectToAction("Index","Jugar") ;
        }

   
    }
}
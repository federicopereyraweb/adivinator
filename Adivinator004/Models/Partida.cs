﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Adivinator.Models
{
    public class Partida
    {
        public Jugador jugador { get; set; }
        public int NroAleatorio { get; set; }
        public int intentosMax = 10;
        public bool gano;
        public List<int> Intentos { get; set; }
        public int intentosPartida = 0;

        public int puntos { get; set; }

        public static int NumeroRandom()
        {
            Random numRand = new Random();
            return numRand.Next(1, 100);

        }
        public static List<int> ClearIntentos()
        {
            return new List<int>();
        }



    }
}